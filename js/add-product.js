var prod_type = "Dvd";

function hideAll() {
    $("#DVD-form").addClass("d-none");
    $("#Book-form").addClass("d-none");
    $("#Furniture-form").addClass("d-none");
}

function clearAll() {
    $('#size').val("");
    $('#weight').val("");
    $('#height').val("");
    $('#width').val("");
    $('#length').val("");
}

$('#productType').on('change', function() {
    hideAll();
    clearAll();
    $("#" + this.value + "-form").removeClass("d-none");
    
    prod_type = this.value.toLowerCase().charAt(0).toUpperCase() + this.value.slice(1).toLowerCase();
});

function save() {
    var variation = {Dvd: [$('#size').val()], Book: [$('#weight').val()], Furniture: [$('#height').val(), $('#width').val(), $('#length').val()]}
    var is_filled = true;

    for(let i = 0; i < variation[this.prod_type].length; i++) {
        if(variation[this.prod_type][i] == "") is_filled = false;
    }

    if($('#sku').val() == "" || $('#name').val() == "" || $('#price').val() == "" || !is_filled) {
        alert("Please, submit required data");
    }
    else {
        $.ajax({
            url: "../php/product.php",
            data: {class: this.prod_type, data: [$('#sku').val(), $('#name').val(), $('#price').val(), variation[this.prod_type]], control_operation: "operate", type_operation: "save_product_type"},
            dataType: "text",
            success: function(result) {
                if(result == "") redirect('./');
                else alert("SKU already exists.");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });
    }
}

function redirect(page) {
    window.location.href = page;
}

$('#price').focusout(function() {
    this.value = parseFloat(this.value).toFixed(2);
});

$('#price').keypress(function(event) {
    if (event.key == 'e' || event.key == 'E' || event.key == '+' || event.key == '-') {
        event.preventDefault();
    }
});

$('#size').keypress(function(event) {
    if (event.key == 'e' || event.key == 'E' || event.key == '+' || event.key == '-') {
        event.preventDefault();
    }
});

$('#weight').keypress(function(event) {
    if (event.key == 'e' || event.key == 'E' || event.key == '+' || event.key == '-') {
        event.preventDefault();
    }
});

$('#height').keypress(function(event) {
    if (event.key == 'e' || event.key == 'E' || event.key == '+' || event.key == '-') {
        event.preventDefault();
    }
});

$('#width').keypress(function(event) {
    if (event.key == 'e' || event.key == 'E' || event.key == '+' || event.key == '-') {
        event.preventDefault();
    }
});

$('#length').keypress(function(event) {
    if (event.key == 'e' || event.key == 'E' || event.key == '+' || event.key == '-') {
        event.preventDefault();
    }
});
