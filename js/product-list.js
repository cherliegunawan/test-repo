'use strict';

const e = React.createElement;

class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.props = {data: props};
    }

    renderCell(data) {
        return e(
            'div', {className: 'col-3', key: 'div-1-' + data['sku']}, e(
                'div', {className: 'cell shadow', key: 'div-2-' + data['sku']}, e(
                    'div', {className: 'container', key: 'div-3-' + data['sku']}, [
                        e('input', {type: 'checkbox', className: 'delete-checkbox', value: JSON.stringify(data), key: 'input-' + data['sku']}, null),
                        e('div', {className: 'text-center', key: 'div-4-' + data['sku']}, [
                            e('p', {className: "m-0", key: 'sku-' + data['sku']}, data['sku']),
                            e('p', {className: "m-0", key: 'name-' + data['name']}, data['name']),
                            e('p', {className: "m-0", key: 'price-' + data['price']}, data['price']),
                            e('p', {className: "m-0", key: 'additional-' + data['additional']}, data['additional'])
                        ])
                    ]
                )
            )
        );
    }
    
    loadData() {
        var cells = [];
        var rows = [];

        var i;

        for (i = 0; i < this.props.data.length; i++) {
            cells.push(this.renderCell(this.props.data[i]));
            if((i + 1) % 4 == 0) {
                rows.push(e('div', {className: "row", key: 'row-' + i}, cells));
                cells = [];
            }
        }

        if(cells.length > 0) rows.push(e('div', {className: "row", key: 'row-' + i}, cells));

        return rows;
    }

    render() {
        return this.loadData();
    }
}

const domContainer = document.querySelector('#product_list');
render_page();

function render_page() {
    $.ajax({
        url: "../php/product.php",
        data: {class: null, data: null, control_operation: "get_all_available_products", type_operation: null},
        dataType: "JSON",
        success: function(result) {
            var data = new ProductList(result);
            ReactDOM.render(data.render(), domContainer);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });
}

var deleteList = [];

$(document.body).on('click', '#delete-product-btn', function(event) {
    var deleteData = [];
    for(let i = 0; i < deleteList.length; i++) {
        deleteData.push(JSON.parse(deleteList[i]));
    }
    
    $.ajax({
        url: "../php/product.php",
        data: {class: null, data: deleteData, control_operation: "delete_product", type_operation: null},
        dataType: "text",
        success: function(result) {
            render_page();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        }
    });
});

$(document.body).on('click', '.delete-checkbox', function(event) {
    if(event.target.checked) deleteList.push(event.target.value)
    else {
        let i = deleteList.indexOf(event.target.value);
        if(i >= 0) {
            deleteList.splice(i,1);
        }
    }
});

function redirect(page) {
    window.location.href = page;
}
