<?php
    require "database.php";

    class ProductController {
        private $class;
        private $data;
        private $operation;
        private $connection;

        function __construct($class, $data, $operation, $connection) {
            $this->connection = $connection;

            $this->class = $class;
            $this->data = $data;
            $this->operation = $operation;
        }

        function operate() {
            $class = $this->class;
            $data = $this->data;
            $operation = $this->operation;
            $connection = $this->connection;

            $new_class = new $class($connection);
            $new_class->with_value($data);
            return $new_class->$operation();
        }

        function get_all_available_products() {
            $dvd = (new Dvd($this->connection))->get_products();
            $book = (new Book($this->connection))->get_products();
            $furniture = (new Furniture($this->connection))->get_products();

            $data = array_merge($dvd, $book, $furniture);

            usort($data, function ($a, $b) {
                return $a['id'] - $b['id'];
            });

            return json_encode($data);
        }

        function delete_product() {
            for($i = 0; $i < count($this->data); $i++) {
                $temp = new $this->data[$i]['type']($this->connection);
                $temp->with_product_data([$this->data[$i]['sku'], $this->data[$i]['name'], $this->data[$i]['price']]);
                $temp->delete_product();
            }
        }
    }

    abstract class Product {
        private $sku;
        private $name;
        private $price;
        private $connection;

        function __construct($connection) {
            $this->connection = $connection;
        }

        function with_product_value($sku, $name, $price) {
            $this->sku = $sku;
            $this->name = $name;
            $this->price = $price;
        }

        function get_sku() {
            return $this->sku;
        }

        function get_name() {
            return $this->name;
        }

        function get_price() {
            return $this->price;
        }

        function get_connection() {
            return $this->connection;
        }

        function set_sku($sku) {
            $this->sku = $sku;
        }

        function set_name($name) {
            $this->name = $name;
        }

        function set_price($price) {
            $this->price = $price;
        }

        //universal product database operations.
        function delete_product() {
            //using deleted_at for soft delete.
            $connection = $this->get_connection();

            $stmt = $connection->prepare("UPDATE products SET deleted_at = current_timestamp() WHERE sku = ?");
            $stmt->bind_param("s", $this->sku);
            $stmt->execute();
            $stmt->close();
        }

        function save_product() {
            $connection = $this->get_connection();

            $stmt = $connection->prepare("INSERT INTO products (sku, name, price) VALUES (?, ?, ?)");
            $stmt->bind_param("ssd", $this->sku, $this->name, $this->price);
            $stmt->execute();
            
            $last_id = $stmt->insert_id;

            $stmt->close();

            return $last_id;
        }

        //type-based product database operations.
        abstract function save_product_type();
        abstract function get_products();
    }

    class Dvd extends Product {
        private $size;

        function __construct($connection) {
            parent::__construct($connection);
        }

        function with_product_data($data) {
            parent::with_product_value($data[0], $data[1], $data[2]);
        }

        function with_value($data) {
            parent::with_product_value($data[0], $data[1], $data[2]);
            $this->size = $data[3][0];
        }

        function get_size() {
            return $this->size;
        }

        function save_product_type() {
            $id = parent::save_product();

            $connection = $this->get_connection();

            $stmt = $connection->prepare("INSERT INTO dvd (product_id, size) VALUES (?, ?)");
            $stmt->bind_param("ii", $id, $this->size);
            $stmt->execute();
            $res = $stmt->error;
            $stmt->close();

            return $res;
        }

        function get_products() {
            $resArray = array();

            $connection = $this->get_connection();

            $stmt = $connection->prepare("SELECT 'Dvd' AS type, products.id AS id, products.sku AS sku, products.name AS name, products.price AS price, CONCAT('Size: ', dvd.size, ' MB') AS additional FROM dvd JOIN products ON dvd.product_id = products.id WHERE products.deleted_at IS NULL");
            $stmt->execute();
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                array_push($resArray, $row);
            }
            $stmt->close();

            return $resArray;
        }
    }

    class Book extends Product {
        private $weight;

        function __construct($connection) {
            parent::__construct($connection);
        }

        function with_product_data($data) {
            parent::with_product_value($data[0], $data[1], $data[2]);
        }

        function with_value($data) {
            parent::with_product_value($data[0], $data[1], $data[2]);
            $this->weight = $data[3][0];
        }

        function get_weight() {
            return $this->weight;
        }

        function save_product_type() {
            $id = parent::save_product();

            $connection = $this->get_connection();

            $stmt = $connection->prepare("INSERT INTO book (product_id, weight) VALUES (?, ?)");
            $stmt->bind_param("ii", $id, $this->weight);
            $stmt->execute();
            $res = $stmt->error;
            $stmt->close();

            return $res;
        }

        function get_products() {
            $resArray = array();

            $connection = $this->get_connection();

            $stmt = $connection->prepare("SELECT 'Book' AS type, products.id AS id, products.sku AS sku, products.name AS name, products.price AS price, CONCAT('Weight: ', book.weight, 'KG') AS additional FROM book JOIN products ON book.product_id = products.id WHERE products.deleted_at IS NULL");
            $stmt->execute();
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                array_push($resArray, $row);
            }
            $stmt->close();

            return $resArray;
        }
    }

    class Furniture extends Product {
        private $height;
        private $width;
        private $length;

        function __construct($connection) {
            parent::__construct($connection);
        }

        function with_product_data($data) {
            parent::with_product_value($data[0], $data[1], $data[2]);
        }

        function with_value($data) {
            parent::with_product_value($data[0], $data[1], $data[2]);
            $this->height = $data[3][0];
            $this->width = $data[3][1];
            $this->length = $data[3][2];
        }

        function get_height() {
            return $this->height;
        }

        function get_width() {
            return $this->width;
        }

        function get_length() {
            return $this->length;
        }

        function save_product_type() {
            $id = parent::save_product();

            $connection = $this->get_connection();

            $stmt = $connection->prepare("INSERT INTO furniture (product_id, height, width, length) VALUES (?, ?, ?, ?)");
            $stmt->bind_param("iiii", $id, $this->height, $this->width, $this->length);
            $stmt->execute();
            $res = $stmt->error;
            $stmt->close();

            return $res;
        }

        function get_products() {
            $resArray = array();

            $connection = $this->get_connection();

            $stmt = $connection->prepare("SELECT 'Furniture' AS type, products.id AS id, products.sku AS sku, products.name AS name, products.price AS price, CONCAT('Dimension: ', furniture.height, 'x', furniture.width, 'x', furniture.length) AS additional FROM furniture JOIN products ON furniture.product_id = products.id WHERE products.deleted_at IS NULL");
            $stmt->execute();
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                array_push($resArray, $row);
            }
            $stmt->close();

            return $resArray;
        }
    }

    $class = $_REQUEST["class"];
    $data = $_REQUEST["data"];
    $control_operation = $_REQUEST["control_operation"];
    $type_operation = $_REQUEST["type_operation"];

    $dbConn = (new Database("localhost", "id17873994_cherlie", "BFB%dM3J=7N_o{Q<", "id17873994_scandiwebtest"))->get_connection();

    $result = new ProductController($class, $data, $type_operation, $dbConn);

    echo $result->$control_operation();

    $dbConn->close();
?>