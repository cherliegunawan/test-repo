<?php
    class Database {
        private $conn;

        function __construct($servername, $username, $password, $dbname) {
            $this->conn = new mysqli($servername, $username, $password, $dbname);
        }

        function get_connection() {
            return $this->conn;
        }
    }
?>